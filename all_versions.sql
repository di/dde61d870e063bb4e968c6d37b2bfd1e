select 
  version, 
  count(version), 
  max(created) as version_count 
from 
  releases 
group by 
  version 
order by 
  version_count desc;