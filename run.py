import csv
import datetime
from collections import defaultdict

import packaging.version

totals = defaultdict(int)

latest = datetime.datetime(1970, 1, 1, 0, 0, 0, 0)

with open('versions.csv') as f:
    reader = csv.reader(f)

    with open('out.csv', 'w') as o:
        writer = csv.writer(o)
        for row in reader:
            version, count, created = row
            t = type(packaging.version.parse(version)).__name__
            writer.writerow([version, count, created, t])
            totals[t] += int(count)

            if t == 'LegacyVersion':
                ts = datetime.datetime.fromisoformat(created)
                if ts > latest:
                    latest = ts

print(totals)
print(latest)